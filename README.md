## Panda coding task ##

Application has been developed with yii2 framework - modern, well designed and modular framework.

After downloading the project please use `composer install` to download all packages
Database used is a remote free service MYSQL server, details are in /config/db.php file
After mounting the project in a web server navigate browser to "project folder"/web/index.php

Authorized user:
Username: admin
Password: admin

Demo user:
Username: demo
Password: demo

To fetch information use CLI `php yii hello/get-people` in project main directory
** Person's name was used to distinct between People when importing data.