<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\People;
use app\models\PersonFilm;
use app\models\PersonSpecie;
use app\models\PersonStarship;
use app\models\PersonVehicle;
use Faker\Provider\Person;
use yii\base\Exception;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller {
    private $errors = [];

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world') {
        echo $message . "\n";

        return ExitCode::OK;
    }


    /**
     * Gets data of the People entity from Swapi API
     */
    public function actionGetPeople() {
        $people = $this->getData("https://swapi.co/api/people/");
        echo "Fetched ".count($people)." people.".PHP_EOL;
        $inserted = $this->insertPeople($people);
        echo "Number of people inserted to database: $inserted people.".PHP_EOL;
        echo "Number of errors: ".count($this->errors).PHP_EOL;
//        print_r($this->errors); //log errors
    }

    /**
     * Fetches all pages data from given URL
     * @param $url
     * @return array
     */
    private function getData($url = null) {
        $data = [];
        try {
            while ($url !== null) {
                $response = $this->fetchPage($url);
                if ($response) {
                    $chunk = json_decode($response,true);
                    $url = $chunk['next'];
                    $data = array_merge($data, $chunk['results']);
                }
            }
        } catch (Exception $e) {
            echo 'get data';
            $this->ExceptionControl($e);
        }
        return $data;
    }

    /**
     * Makes request to fetch specific page data
     * @param $url
     * @return mixed
     */
    private function fetchPage($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Checks if Person already exists in database
     * @param $person_name
     * @return bool
     */
    private function checkPersonExist($person_name){
        if(!empty($person_name) && People::find()->where(['name'=>$person_name])->one() !== null){
            return true;
        }
        return false;
    }

    /**
     * Inserts People and People relationships data into database.
     * @param $peopleData
     * @return int - number of inserted People
     */
    private function insertPeople($peopleData) {
        $completedCounter = 0;
        $skippedCounter = 0;
        $relationError = false;
        $model = null;
        $modelRelationSuccess = [];
        try {
            foreach (is_array($peopleData) ? $peopleData : [] as $k => $personData) {
                if($this->checkPersonExist($personData['name'])){
                    $skippedCounter++;
                    continue;
                }
                if(!empty($modelRelationSuccess)){
                    $modelRelationSuccess = [];
                }
                $relationError = false;
                $filmsData = $personData['films'];
                unset($personData['films']);
                $speciesData = $personData['species'];
                unset($personData['species']);
                $vehiclesData = $personData['vehicles'];
                unset($personData['vehicles']);
                $starshipsData = $personData['starships'];
                unset($personData['starships']);
                $person = new People($personData);
                if ($person->hasErrors()) {
                    $this->errors['Person'][$k] = $person->getErrors();
                } else if($person->save()){
                    $modelRelationSuccess[PersonFilm::class] = $this->insertPersonRelation($person->id, ['className'=>PersonFilm::class, 'modelName'=>'PersonFilm', 'fieldName'=>'film'], $filmsData);
                    $modelRelationSuccess[PersonSpecie::class] = $this->insertPersonRelation($person->id, ['className'=>PersonSpecie::class, 'modelName'=>'PersonSpecie', 'fieldName'=>'specie'], $speciesData);
                    $modelRelationSuccess[PersonVehicle::class] = $this->insertPersonRelation($person->id, ['className'=>PersonVehicle::class, 'modelName'=>'PersonVehicle', 'fieldName'=>'vehicle'], $vehiclesData);
                    $modelRelationSuccess[PersonStarship::class] = $this->insertPersonRelation($person->id, ['className'=>PersonStarship::class, 'modelName'=>'PersonStarship', 'fieldName'=>'starship'], $starshipsData);
                    foreach ($modelRelationSuccess as $relationSuccess){
                        if(!$relationSuccess){
                            $relationError = true;
                            break;
                        }
                    }
                    if(!$relationError){
                        $completedCounter++;
                    }
                }
            }
            if($skippedCounter){
                echo "Skipped $skippedCounter people.".PHP_EOL;
            }
            return $completedCounter;
        } catch (Exception $e) {
            echo 'get insertPeople base';
            $this->ExceptionControl($e);
        } catch (\yii\db\Exception $e) {
            echo 'get insertPeople db';
            $this->ExceptionControl($e);
        }
    }

    /**
     * Inserts Person's relationship data to database
     * @param $personId
     * @param $config
     *      $config[className] - string Relationship class name
     *      $config[modelName] - string Relationship model name
     *      $config[fieldName] - string Relationship field name
     * @param array $data
     * @return bool - if insert succeeds
     */
    private function insertPersonRelation($personId, $config, $data = []) {
        $success = true;
        foreach ($data as $k => $item) {
            $model = new $config['className'](['personId' => $personId, $config['fieldName'] => $item]);
            if ($model->hasErrors()) {
                $this->errors[$config['modelName']]['key'] = $model->getErrors();
                $success = false;
            } else {
                $model->save();
            }
        }
        return $success;
    }

    /**
     * Exception management, could be used to log exceptions
     * @param \Exception $e
     */
    private function ExceptionControl(\Exception $e){
        echo PHP_EOL."### Exception: {$e->getMessage()} at {$e->getFile()} line {$e->getLine()}";
        //log...
    }

}
