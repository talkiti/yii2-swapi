<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "people".
 *
 * @property int $id
 * @property string $name
 * @property string $birth_year
 * @property string $gender
 * @property string $height
 * @property string $mass
 * @property string $skin_color
 * @property string $hair_color
 * @property string $eye_color
 * @property string $url
 * @property string $created
 * @property string $edited
 *
 * @property PersonFilm[] $personFilms
 * @property PersonSpecie[] $personSpecies
 * @property PersonStarship[] $personStarships
 * @property PersonVehicle[] $personVehicles
 */
class People extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'people';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created', 'edited'], 'safe'],
            [['name', 'url', 'homeworld'], 'string', 'max' => 200],
            [['birth_year', 'gender', 'height', 'mass', 'skin_color', 'hair_color', 'eye_color'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birth_year' => 'Birth Year',
            'gender' => 'Gender',
            'height' => 'Height',
            'mass' => 'Mass',
            'skin_color' => 'Skin Color',
            'hair_color' => 'Hair Color',
            'eye_color' => 'Eye Color',
            'url' => 'Url',
            'homeworld' => 'Homeworld',
            'created' => 'Created',
            'edited' => 'Edited',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonFilms()
    {
        return $this->hasMany(PersonFilm::className(), ['personId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonSpecies()
    {
        return $this->hasMany(PersonSpecie::className(), ['personId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonStarships()
    {
        return $this->hasMany(PersonStarship::className(), ['personId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonVehicles()
    {
        return $this->hasMany(PersonVehicle::className(), ['personId' => 'id']);
    }
}
