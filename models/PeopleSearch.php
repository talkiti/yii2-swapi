<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\People;

/**
 * PeopleSearch represents the model behind the search form of `app\models\People`.
 */
class PeopleSearch extends People
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'birth_year', 'gender', 'height', 'mass', 'skin_color', 'hair_color', 'eye_color', 'url', 'created', 'edited'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = People::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
            'edited' => $this->edited,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'birth_year', $this->birth_year])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'height', $this->height])
            ->andFilterWhere(['like', 'mass', $this->mass])
            ->andFilterWhere(['like', 'skin_color', $this->skin_color])
            ->andFilterWhere(['like', 'hair_color', $this->hair_color])
            ->andFilterWhere(['like', 'eye_color', $this->eye_color])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
