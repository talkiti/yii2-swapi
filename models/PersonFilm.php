<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_film".
 *
 * @property int $personId
 * @property string $film
 *
 * @property People $person
 */
class PersonFilm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person_film';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['personId', 'film'], 'required'],
            [['personId'], 'integer'],
            [['film'], 'string', 'max' => 200],
            [['personId', 'film'], 'unique', 'targetAttribute' => ['personId', 'film']],
            [['personId'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['personId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'personId' => 'Person ID',
            'film' => 'Film',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(People::className(), ['id' => 'personId']);
    }
}
