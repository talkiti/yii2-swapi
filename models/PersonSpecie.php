<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_specie".
 *
 * @property int $personId
 * @property string $specie
 *
 * @property People $person
 */
class PersonSpecie extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person_specie';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['personId', 'specie'], 'required'],
            [['personId'], 'integer'],
            [['specie'], 'string', 'max' => 200],
            [['personId', 'specie'], 'unique', 'targetAttribute' => ['personId', 'specie']],
            [['personId'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['personId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'personId' => 'Person ID',
            'specie' => 'Specie',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(People::className(), ['id' => 'personId']);
    }
}
