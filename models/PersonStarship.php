<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_starship".
 *
 * @property int $personId
 * @property string $starship
 *
 * @property People $person
 */
class PersonStarship extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person_starship';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['personId', 'starship'], 'required'],
            [['personId'], 'integer'],
            [['starship'], 'string', 'max' => 200],
            [['personId', 'starship'], 'unique', 'targetAttribute' => ['personId', 'starship']],
            [['personId'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['personId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'personId' => 'Person ID',
            'starship' => 'Starship',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(People::className(), ['id' => 'personId']);
    }
}
