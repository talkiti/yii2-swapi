<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person_vehicle".
 *
 * @property int $personId
 * @property string $vehicle
 *
 * @property People $person
 */
class PersonVehicle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person_vehicle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['personId', 'vehicle'], 'required'],
            [['personId'], 'integer'],
            [['vehicle'], 'string', 'max' => 200],
            [['personId', 'vehicle'], 'unique', 'targetAttribute' => ['personId', 'vehicle']],
            [['personId'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['personId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'personId' => 'Person ID',
            'vehicle' => 'Vehicle',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(People::className(), ['id' => 'personId']);
    }
}
