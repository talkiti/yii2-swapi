<?php

/* @var $this yii\web\View */

$this->title = 'yii2-swapi';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to yii2-Swapi!</h1>

        <p class="lead">Please login to watch or manipulate data.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>All the Star Wars data you've ever wanted</h2>

                <p>Planets, Spaceships, Vehicles, People, Films and Species
                    From all SEVEN Star Wars films
                    Now with The Force Awakens data!</p>

                <p><a class="btn btn-default" href="https://swapi.co/">SWAPI - The Star Wars API &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>All the Star Wars data you've ever wanted</h2>

                <p>Planets, Spaceships, Vehicles, People, Films and Species
                    From all SEVEN Star Wars films
                    Now with The Force Awakens data!</p>

                <p><a class="btn btn-default" href="https://swapi.co/">SWAPI - The Star Wars API &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>All the Star Wars data you've ever wanted</h2>

                <p>Planets, Spaceships, Vehicles, People, Films and Species
                    From all SEVEN Star Wars films
                    Now with The Force Awakens data!</p>

                <p><a class="btn btn-default" href="https://swapi.co/">SWAPI - The Star Wars API &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
